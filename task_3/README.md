# Instructions

## Installation

Create conda environment:
```sh
conda create --name openmmlab python=3.8 -y
conda activate openmmlab

pip install torch==2.1.2 torchvision==0.16.2 torchaudio==2.1.2 --index-url https://download.pytorch.org/whl/cu118
pip install openmim
pip install mmengine
pip install mmcv==2.1.0 -f https://download.openmmlab.com/mmcv/dist/cu118/torch2.1/index.html
mim install 'mmdet>=3.0.0'

git clone https://github.com/open-mmlab/mmdetection3d.git -b dev-1.x
cd mmdetection3d
pip install -v -e .
```

Go back to parent directory!

Install `pypcd`:
```sh
git clone https://github.com/klintan/pypcd.git
cd pypcd

python setup.py install
```

Go back to parent directory!

Convert dataset:
```sh
git clone https://github.com/AIR-THU/DAIR-V2X.git
cd DAIR-V2X

PYTHONPATH=. python tools/dataset_converter/dair2kitti.py --source-root ../single-infrastructure-side --target-root ../data/kitti --split-path ./data/split_datas/single-infrastructure-split-data.json --label-type lidar --sensor-view infrastructure
```

Prepare dataset and configs:

```sh
cd mmdetection3d

# apply patches
git apply ../patches/mmdetection3d/*.patch

# create dataset
PYTHONPATH=. python tools/create_data.py kitti --root-path ./data/kitti --out-dir ./data/kitti --extra-tag kitti

# copy configs
cp -r ../configs ./configs/custom
```

## Training

Append the following parameters *at the end* of the training command if saving checkpoints is desired:
```
 --cfg-options default_hooks.checkpoint.interval=1
```

The scripts we used to run the training can be found in the `slurm_scripts` directory.

### MVX-Net

```sh
python tools/train.py ./configs/custom/mvxnet.py --workdir ./work_dirs/mvx
```

### Point Pillars

```sh
python tools/train.py ./configs/custom/pointpillars.py --workdir ./work_dirs/pp
```

### ImVoxelNet

```sh
python tools/train.py ./configs/custom/imvoxelnet.py --workdir ./work_dirs/ivn
```

## Evaluation

Go into the `eval` folder and take a look at the `README.md` inside.
