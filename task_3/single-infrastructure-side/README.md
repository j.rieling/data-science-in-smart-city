## Dataset Format

Source: https://thudair.baai.ac.cn/roadtest

| Data | Introduction to data |
|------|----------------------|
| velodyne/xxxxxx.pcd | Infrastructure point cloud in Infrastructure Virtual LiDAR Coordinate System |
| image/xxxxxx.jpg | Infrastructure image sync with Infrastructure LiDAR |
| calib/virtuallidar_to_camera/xxxxxx.json | Extrinsic matrix from Infrastructure Virtual LiDAR Coordinate System to Infrastructure Camera Coordinate System |
| calib/camera_intrinsic/xxxxxx.json | Infrastructure Camera intrinsic parameters |
| label/camera/xxxxxxx.json | Labeled data in Infrastructure Virtual LiDAR Coordinate System fitting objects in image based on image frame time |
| label/virtuallidar/xxxxxx.json | Labeled data in Infrastructure Virtual LiDAR Coordinate System fitting objects in point cloud based on point cloud frame time |
| data_info.json | Relevant index information of the Infrastructure data |
