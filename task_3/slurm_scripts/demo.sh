#!/bin/bash
#SBATCH --job-name=demo
#SBATCH -t 00:15:00                  # set time limit
#SBATCH -p grete:interactive         # -p grete:shared for training, -p grete:interactive for debugging

#SBATCH -G V100:1                    # take 1 GPU, see https://www.hlrn.de/doc/display/PUB/GPU+Usage for more options
##SBATCH --mem-per-gpu=5G             # setting the right constraints for the splitted gpu partitions

#SBATCH --nodes=1                    # total number of nodes
#SBATCH --ntasks=1                   # total number of tasks
#SBATCH --cpus-per-task=4            # number cores per task

## mail settings for job info
#SBATCH --mail-type=all              # send mail when job begins and ends
#SBATCH --mail-user=leander.booms01@stud.uni-goettingen.de  # mailaddress
#SBATCH --output=./slurm_files/%x.out        # where to write output, %x give job name, %j names job id
#SBATCH --error=./slurm_files/%x.err         # where to write slurm error

module load anaconda3
module load cuda

source activate openmmlab

python mmdetection3d/demo/pcd_demo.py mmdetection3d/demo/data/kitti/000008.bin mmdetection3d/pointpillars_hv_secfpn_8xb6-160e_kitti-3d-car.py mmdetection3d/hv_pointpillars_secfpn_6x8_160e_kitti-3d-car_20220331_134606-d42d15ed.pth
