This directory contains the slurm scripts we used to run our training, evaluation and benchmarks on the GPU cluster.

To reproduce our workflow, simply copy them into the parent directory and modify them according to your requirements.

Be sure to follow instructions in the `README.md` from the parent directory first.

Run a script:
```
sbatch <script.sh>
```
