#!/bin/bash
#SBATCH --job-name=test
#SBATCH -t 01:00:00                  # set time limit
#SBATCH -p grete:interactive         # -p grete:shared for training, -p grete:interactive for debugging

#SBATCH -G V100:1                    # take 1 GPU, see https://www.hlrn.de/doc/display/PUB/GPU+Usage for more options
##SBATCH --mem-per-gpu=5G             # setting the right constraints for the splitted gpu partitions

#SBATCH --nodes=1                    # total number of nodes
#SBATCH --ntasks=1                   # total number of tasks
#SBATCH --cpus-per-task=4            # number cores per task

## mail settings for job info
#SBATCH --mail-type=all              # send mail when job begins and ends
#SBATCH --mail-user=leander.booms01@stud.uni-goettingen.de  # mailaddress
#SBATCH --output=./slurm_files/%x.out        # where to write output, %x give job name, %j names job id
#SBATCH --error=./slurm_files/%x.err         # where to write slurm error

module load anaconda3
module load cuda

source activate openmmlab

cd mmdetection3d
python tools/test.py --work-dir ../workdir configs/mvxnet/mvxnet_fpn_dv_second_secfpn_8xb2-80e_kitti-3d-3class.py ../workdir/epoch_2.pth --show --show-dir showdir
