#!/bin/bash
#SBATCH --job-name=test-mvx
#SBATCH -t 30:00:00                  # set time limit
#SBATCH -p grete:interactive         # -p grete:shared for training, -p grete:interactive for debugging

#SBATCH -G V100:1                    # take 1 GPU, see https://www.hlrn.de/doc/display/PUB/GPU+Usage for more options
##SBATCH --mem-per-gpu=5G             # setting the right constraints for the splitted gpu partitions

#SBATCH --nodes=1                    # total number of nodes
#SBATCH --ntasks=1                   # total number of tasks
#SBATCH --cpus-per-task=4            # number cores per task

## mail settings for job info
#SBATCH --mail-type=all              # send mail when job begins and ends
#SBATCH --mail-user=leander.booms01@stud.uni-goettingen.de  # mailaddress
#SBATCH --output=./slurm_files/%x-%j.out        # where to write output, %x give job name, %j names job id
#SBATCH --error=./slurm_files/%x-%j.err         # where to write slurm error

module load anaconda3
module load cuda

source activate openmmlab

cd mmdetection3d
for i in {8..40}; do
	python tools/test.py ./configs/custom/mvxnet.py work_dirs/mvx/epoch_$i.pth \
		--work-dir ./work_dirs/mvx-test \
		--task mono_det --show-dir epoch_$i-mono_det \
		--cfg-options test_evaluator.pklfile_prefix=work_dirs/mvx-test/epoch_$i
done
