## Evaluation

The `parse_log` script takes the work directories where the training logs are stored as command-line argument and stores evaluation metrics in CSVs inside a folder of the same name.

For example after all training has been done, we can run the following:
```
./parse_log mvx pp ivn
```

The `notebook.ipynb` contains code to visualize the stored evaluation data.
