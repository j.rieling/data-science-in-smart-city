# Task 2 -- Report: Simple exercise of 3D reconstruction

- Leander Booms, `leander.booms01@stud.uni-goettingen.de`, 21879636
- Jonas Adrian Rieling, `j.rieling@stud.uni-goettingen.de`, 21880237

## Overview

In this task we did several 3D reconstructions of buildings or other objects using images fed into a pipeline utilizing OpenMVG and OpenMVS.
We build OpenMVG and OpenMVS in a containerized reproducible manner using a Containerfile with `podman` which is a daemonless drop-in replacement for `docker`. This helps easily reproducing our results on different machines.
The build process is very similar to the given build instructions and should produce the same results.

## Pipeline

Our reconstruction pipeline is very similar to the one provided in the instructions for this task, with just a few adjustments.

In `openMVG_main_SfMInit_ImageListing`, instead of specifying focal length manually with `-f`, we rely on the automatic pixel focal computation from JPEG EXIF metadata and a database of camera sensor width.

The resulting pipeline then looks as follows:

```sh
# openMVG
mkdir -p output/matches
openMVG_main_SfMInit_ImageListing -i images/ \
 -d /opt/openMVG/src/openMVG/exif/sensor_width_database/sensor_width_camera_database.txt \
 -o output/matches
openMVG_main_ComputeFeatures -i output/matches/sfm_data.json -o output/matches
openMVG_main_ComputeMatches -i output/matches/sfm_data.json -o output/matches/matches.txt
openMVG_main_SfM -i output/matches/sfm_data.json -o output/out_Incremental_Reconstruction \
 --match_file output/matches/matches.txt
openMVG_main_ExportUndistortedImages -i output/matches/sfm_data.json \
 -o output/out_Incremental_Reconstruction/images

# openMVS
cd output/out_Incremental_Reconstruction
openMVG_main_openMVG2openMVS -i sfm_data.bin -o scene.mvs
DensifyPointCloud scene.mvs
ReconstructMesh scene_dense.mvs
RefineMesh scene_dense_mesh.mvs
TextureMesh scene_dense_mesh_refine.mvs
```

The final result is usually stored as a file named `scene_dense_mesh_refine_texture.mvs` in the directory `output/out_Incremental_Reconstruction` relative to the folder containing our `images`.
At the same time this creates a file named `scene_dense_mesh_refine_texture.ply` which can be viewed by a model viewer like [MeshLab](https://www.meshlab.net/).

## Results

Our pictures were taken using a Canon EOS 2000D, which are captured at an original resolution of 6000x4000 and downscaled to a resolution of 3000x2000 for reconstruction.

### Sceaux castle

Source: https://github.com/openMVG/ImageDataset_SceauxCastle

Photographer: Copyright 2012 Pierre MOULON http://imagine.enpc.fr/~moulonp/

![Sceaux castle images](images/SceauxCastle_images.png)
![Sceaux castle model](images/SceauxCastle_model.png)

### Mathematical Institute

Location: Bunsenstr. 3-5, Göttingen, Germany [(OpenStreetMap)](https://www.openstreetmap.org/way/222759245)

The biggest of our reconstructions with the most amount of pictures.

![MI images](images/MI_images.png)
![MI model](images/MI_model.png)

### Institute of Numerical and Applied Mathematics

Location: Lotzestr. 16-18, Göttingen, Germany [(OpenStreetMap)](https://www.openstreetmap.org/way/222776941)

This was quite well reconstructed considering its low amount of pictures.

![NAM images](images/NAM_images.png)
![NAM model](images/NAM_model.png)

### Learning and Study Building (LSG)

Location: Platz der Göttinger Sieben 3a, Göttingen, Germany [(OpenStreetMap)](https://www.openstreetmap.org/way/215581268)

The quality of this model is rather low. One reason for this is the low amount of pictures from different angles and the trees in the area. We already had to remove a few pictures from our set because the presence of trees would destroy the reconstruction.

![LSG images](images/LSG_images.png)
![LSG model](images/LSG_model.png)

### Stone block

Location: 51°32'20.6"N 9°56'08.4"E

This stone block is the only model which is captured and reconstructed from all sides.

![Block images](images/block_images.png)
![Block model](images/block_model.png)

## Observations and Challenges

We hit multiple challenges with reconstructing our own images. One of our key observations is the fact that just having more images is not always better. We also experienced a quite a few bugs in the software like memory leaks and random crashes due to [segmentation fault](https://en.wikipedia.org/wiki/Segmentation_fault) or indeterministic behaviour in which the same procedure gives sometimes different results.

A large number of images with a high resolution can massively increase required RAM which makes it infeasible to reconstruct. Using less, downscaled pictures turned out to be better for us, as it is less prone to crashes and requires less RAM.

Another of our observations is that the peripheral areas of models are often not reconstructed in the model, even if they are present on pictures. Trees are especially destructive in the process of reconstruction as seen in the LSG example. We also noticed that the OpenMVS step in the pipeline for creating the mesh from the point cloud is often much more computationally expensive and therefore more time-consuming than the OpenMVG step for reconstructing the dense point cloud in the first place. This is especially the case for a large amount of pictures with high-resolution.
