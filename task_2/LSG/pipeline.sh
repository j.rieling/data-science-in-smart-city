# openMVG
mkdir -p output/matches
openMVG_main_SfMInit_ImageListing -i images/ -d /opt/openMVG/src/openMVG/exif/sensor_width_database/sensor_width_camera_database.txt -o output/matches
openMVG_main_ComputeFeatures -i output/matches/sfm_data.json -o output/matches
openMVG_main_ComputeMatches -i output/matches/sfm_data.json -o output/matches/matches.txt
openMVG_main_SfM -i output/matches/sfm_data.json -o output/out_Incremental_Reconstruction --match_file output/matches/matches.txt
openMVG_main_ExportUndistortedImages -i output/matches/sfm_data.json -o output/out_Incremental_Reconstruction/images

# openMVS
cd output/out_Incremental_Reconstruction
openMVG_main_openMVG2openMVS -i sfm_data.bin -o scene.mvs
DensifyPointCloud scene.mvs
ReconstructMesh scene_dense.mvs
RefineMesh scene_dense_mesh.mvs
TextureMesh scene_dense_mesh_refine.mvs
