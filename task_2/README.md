# Instructions

## OpenMVG/OpenMVS

Install `podman`, see https://podman.io/docs/installation

Build image:
```sh
podman build --build-arg="MAKE_JOBS=12" . -t openmvg:latest
```

### Example

Clone example images:
```sh
git clone https://github.com/openMVG/ImageDataset_SceauxCastle.git
```

Run reconstruction pipeline:
```sh
podman run --rm -v .:/opt/workdir -w /opt/workdir/ImageDataset_SceauxCastle -it openmvg:latest sh ../pipeline.sh
```

For the other examples, `cd` into that directory and run `make`.

You might need to install [ImageMagick](https://imagemagick.org/) and [GNU Make](https://www.gnu.org/software/make/).

## MeshLab

Install `flatpak` with Flathub remote, see https://flathub.org/setup

Install MeshLab:
```sh
flatpak install flathub net.meshlab.MeshLab
```

View reconstructed mesh:
```sh
flatpak run net.meshlab.MeshLab ImageDataset_SceauxCastle/output/out_Incremental_Reconstruction/scene_dense_mesh_refine_texture.ply
```
