# Setup

## Using `conda`

For a reproducible setup use:
```sh
$ conda env create -f environment.yml
```

Alternatively, create a virtual environment and install packages manually:
```sh
$ conda create -n smartcity
$ conda activate smartcity
$ ./setup
```

## Using `pip`

*(Recommended)* Create a virtual environment using `venv`:
```sh
$ python3 -m venv .venv
$ source .venv/bin/activate
```

Install dependencies:
```sh
$ pip install -r requirements.txt
```
