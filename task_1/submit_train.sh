#!/bin/bash
#SBATCH --job-name=run-gnn-notebook
#SBATCH -t 04:30:00                  # set time limit
#SBATCH -p grete:interactive         # -p grete:shared for training, -p grete:interactive for debugging

#SBATCH -G V100:1                    # take 1 GPU, see https://www.hlrn.de/doc/display/PUB/GPU+Usage for more options
##SBATCH --mem-per-gpu=5G             # setting the right constraints for the splitted gpu partitions

#SBATCH --nodes=1                    # total number of nodes
#SBATCH --ntasks=1                   # total number of tasks
#SBATCH --cpus-per-task=4            # number cores per task

## mail settings for job info
#SBATCH --mail-type=all              # send mail when job begins and ends
#SBATCH --mail-user=leander.booms01@stud.uni-goettingen.de  # mailaddress
#SBATCH --output=./slurm_files/slurm-%x-%j.out        # where to write output, %x give job name, %j names job id
#SBATCH --error=./slurm_files/slurm-%x-%j.err         # where to write slurm error

module load anaconda3
module load cuda
source activate smartcity

# Printing out some info.
echo "Submitting job with sbatch from directory: ${SLURM_SUBMIT_DIR}"
#echo "Home directory: ${HOME}"
#echo "Working directory: $PWD"
echo "Current node: ${SLURM_NODELIST}"

# Run the script(s):
jupyter nbconvert --to notebook --execute report_1.ipynb --ExecutePreprocessor.kernel_name=python3 --output executed_report_1.ipynb

